#!/usr/bin/env bash

# install vim
aptitude -y install vim > /dev/null
echo "Vim installed"

# install ruby and rails
aptitude -y install gnupg2 > /dev/null
gpg2 --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 > /dev/null
\curl -sSL https://get.rvm.io | bash -s stable --rails --ruby > /dev/null
source /usr/local/rvm/scripts/rvm > /dev/null
echo "Ruby and Rails installed"

# install sass and compass
gem install compass > /dev/null
echo "Sass and Compass installed"

# install apache
aptitude -y install apache2 > /dev/null
a2enmod rewrite
cat > /etc/apache2/sites-available/000-default.conf <<- EOM
<VirtualHost *:80>
  # The ServerName directive sets the request scheme, hostname and port that
  # the server uses to identify itself. This is used when creating
  # redirection URLs. In the context of virtual hosts, the ServerName
  # specifies what hostname must appear in the request's Host: header to
  # match this virtual host. For the default virtual host (this file) this
  # value is not decisive as it is used as a last resort host regardless.
  # However, you must set it for any further virtual host explicitly.
  #ServerName www.example.com

  ServerAdmin webmaster@localhost
  DocumentRoot /var/www/html
  <Directory /var/www/>
    Options Indexes FollowSymLinks MultiViews
    AllowOverride all
    Order allow,deny
    allow from all
  </Directory>

  # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
  # error, crit, alert, emerg.
  # It is also possible to configure the loglevel for particular
  # modules, e.g.
  #LogLevel info ssl:warn

  ErrorLog ${APACHE_LOG_DIR}/error.log
  CustomLog ${APACHE_LOG_DIR}/access.log combined

  # For most configuration files from conf-available/, which are
  # enabled or disabled at a global level, it is possible to
  # include a line for only one particular virtual host. For example the
  # following line enables the CGI configuration for this host only
  # after it has been globally disabled with "a2disconf".
  #Include conf-available/serve-cgi-bin.conf
</VirtualHost>
EOM
service apache2 restart
echo "Apache installed"

# install php
aptitude -y install php5 libapache2-mod-php5 php5-mcrypt > /dev/null
php5enmod mcrypt
echo "PHP installed"

# install mysql
echo "mysql-server mysql-server/root_password password root" | sudo debconf-set-selections
echo "mysql-server mysql-server/root_password_again password root" | sudo debconf-set-selections
aptitude -y install mysql-server php5-mysql > /dev/null
echo "MySql installed"

# install memcached
aptitude -y install php5-memcached memcached > /dev/null
echo "Memcached installed"

# install phpmyadmin
echo "phpmyadmin phpmyadmin/dbconfig-install boolean true" | sudo debconf-set-selections
echo "phpmyadmin phpmyadmin/app-password-confirm password root" | sudo debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/admin-pass password root" | sudo debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/app-pass password root" | sudo debconf-set-selections
echo "phpmyadmin phpmyadmin/reconfigure-webserver multiselect none" | sudo debconf-set-selections
aptitude -y install phpmyadmin > /dev/null
echo "phpMyAdmin installed"

# install composer
curl -sS https://getcomposer.org/installer | php > /dev/null
mv composer.phar /usr/local/bin/composer > /dev/null
echo "Composer installed"

# install git
aptitude -y install git > /dev/null
echo "Git installed"

# install nodejs
curl -sL https://deb.nodesource.com/setup | sudo bash - > /dev/null
aptitude install -y nodejs > /dev/null
echo "NodeJS installed"

# install npm
aptitude -y install npm > /dev/null
echo "NPM installed"

# install typescript
npm install -g typescript > /dev/null
echo "TypeScript installed"

# install bower
npm install -g bower > /dev/null
echo "Bower installed"

# install grunt
npm install -g grunt-cli > /dev/null
echo "Grunt installed"

# install gulp
npm install -g gulp > /dev/null
npm install -g --save-dev gulp-util > /dev/null
npm install -g --save-dev gulp-sass > /dev/null
npm install -g --save-dev gulp-compass > /dev/null
npm install -g --save-dev gulp-autoprefixer > /dev/null
npm install -g --save-dev gulp-minify-css > /dev/null
npm install -g --save-dev gulp-clean > /dev/null
npm install -g --save-dev gulp-concat > /dev/null
npm install -g --save-dev gulp-uglify > /dev/null
npm install -g --save-dev gulp-rename > /dev/null
npm install -g --save-dev gulp-filesize > /dev/null
npm install -g --save-dev gulp-changed > /dev/null
echo "Gulp installed"

gem install foundation > /dev/null
echo "Foundation installed"

gem install bundler > /dev/null
echo "Bundler installed"
